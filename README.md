# Computer Vision

This repo contains all the code produced for a Computer Vision course.
The repo includes two projects, the first is based on traditional CV techniques implemented with OpenCV in Python. The second project is based on Machine Learning, mainly on Neural Networks, and includes fine-tuning of several SoTA models, as well as development of a specific architeture based on CNNs for the task.


## Autonomous Coin Counter

This is a project to count coins using computer vision. I implemented traditional techniques for segmentation and identification, such as the use of linear filters, thresholding, watershed, HSV identification, Hough Transforms as well as intrinsic parameters calibration.

## Solar Panel Anomaly Detector

This project consisted in classifying an IR image of a solar panels, with the goal of identifying anomalies. The dataset included 12 classes. This work was based on a scientific paper that published the dataset. I was able to improve classification accuracy for specific classes which had very low performance on the original paper.
The work consisted in processing the data, development of a CNN based model and training, validating and testing the model. Additionally, known models such as EfficientNet-B7 and VGG-16 were fine tuned to benchmark my model's performance

